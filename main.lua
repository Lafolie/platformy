--load libs
class = require "lib.slither"
vector = require "lib.vector"
cache = require "lib.cache"

require "class.game"
-- local platformy --to global, or not to global?

love.load = function()
	math.clamp = function(l, v, h)
		return math.max(math.min(v, h), l)
	end
	
	platformy = Game()	
end

love.update = function(dt)
	platformy:update(dt)
end

love.draw = function()
	platformy:draw()
end

--m&k
love.textinput = function(text)
	platformy:textinput(text)
end

love.keypressed = function(key, unicode)
	platformy:keypressed(key, unicode)
end

love.keyreleased = function(key)
	platformy:keyreleased(key)
end

love.mousepressed = function(x, y, button)
	platformy:mousepressed(x, y, button)
end

love.mousereleased = function(x, y, button)
	platformy:mousereleased(x, y, button)
end

love.mousefocus = function(f)
	platformy:mousefocus(f)
end

--controller
love.joystickadded = function(joystick)
	platformy:joystickadded(joystick)
end

love.joystickremoved = function(joystick)
	platformy:joystickremoved(joystick)
end

love.gamepadpressed = function(joystick, button)
	platformy:gamepadpressed(joystick, button)
end

love.gamepadreleased = function(joystick, button)
	platformy:gamepadreleased(joystick, button)
end

--application
love.focus = function(f)
	platformy:focus(f)
end

love.visible = function(v)
	platformy:visible(v)
end

love.quit = function()
	platformy:quit()
end