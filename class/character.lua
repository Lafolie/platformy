--[[
	Character class designed for controllable Entities (PCs, NPCs, Enemies...)
]]
require "class.entity"

class "Character" (Entity) {
	__init__ = function(self, pos)
		Entity.__init__(self, pos)
		self.input = {left = false, right = false, jump = false}
		self.jumpVelocity = 290
		self.jumpMax = 1
		self.jumpCurrent = 0
		self.jumpTime = 0.5
	end,

	update = function(self, dt, map)
		--handle input
		self:movement(dt)
		self:jump(dt)

		--update the Entity
		Entity.update(self, dt, map)
	end,

	movement = function(self, dt)
		local move = vector()
		move.x = self.input.left == true and move.x-1 or move.x
		move.x = self.input.right == true and move.x+1 or move.x

		move:normalize_inplace()
		self.vel = self.vel+move*self.accel*dt
	end,

	jump = function(self, dt)
		if self.input.jump and self.jumpMax > self.jumpCurrent and self.jumpTime > 0 then
			self.vel.y = -self.jumpVelocity
			self.jumpCurrent = self.jumpCurrent+1
			self.jumpTime = self.jumpTime-dt
		end
	end,

	jumpReleased = function(self, dt)
		self.vel.y = self.vel.y*0.6
		self.jumpTime = 0.5
	end

}