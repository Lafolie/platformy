require "class.rect"
require "class.tileset"

local vecOne = vector(1, 1)

class "Map" (Rect, Tileset) {
	__init__ = function(self, fileName)
		local file = love.filesystem.load("map/" .. fileName)()
		Rect.__init__(self, 0, 0, file.width, file.height)
		Tileset.__init__(self, file.tileset)

		self.layer = file.layer
		self.tileSize = file.tileSize
		
		--render batches
		self.batch = {}
		self.batchAnim = {} --table of spritebatches for animated tiles (currently unused)
		local area = self.size.x*self.size.y
		for z, layer in ipairs(self.layer) do
			local newBatch = love.graphics.newSpriteBatch(self.image, area, "static")
			table.insert(self.batch, newBatch)
			newBatch:bind()
			for y, row in ipairs(layer) do
				for x, tile in ipairs(row) do
					if tile ~= 0 then
						newBatch:add(self.quad[tile], (x-1)*self.tileSize, (y-1)*self.tileSize)
					end
				end
			end
			newBatch:unbind()
		end
		love.graphics.setCanvas()
	end,

	draw = function(self)
		for k, v in ipairs(self.batch) do
			love.graphics.draw(v, self.pos.x, self.pos.y)
		end
	end,

	getTile = function(self, x, y, z)
		z = z or 1
		x, y = math.clamp(1, x, self.size.x), math.clamp(1, y, self.size.y)
		local tileid = self.layer[z][y][x]
		return {solid = self.tileData[tileid].solid, pos = self:tileToWorld(vector(x, y))}
	end,

	getSample = function(self, pos, size)
		local start, finish = self:worldToTile(pos:clone()), self:worldToTile(pos+size)
		local sample = {}
		for y = start.y, finish.y do
			table.insert(sample, {})
			local row = #sample
			for x = start.x, finish.x do
				table.insert(sample[row], self:getTile(x, y))
			end
		end

		return sample
	end,

	worldToTile = function(self, pos)
		pos.x, pos.y = math.floor(pos.x/self.tileSize), math.floor(pos.y/self.tileSize)
		return pos+vecOne
	end,

	tileToWorld = function(self, pos)
		return (pos-vecOne)*self.tileSize
	end
}