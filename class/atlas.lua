--[[
	Atlases are generic tile sheet structures that can be used for animations, maps, gui styles etc.
]]

class "Atlas" {
	__init__ = function(self, fileName, grid, pad)
		print(fileName)
		self.image = cache.image(fileName)
		local size = vector(self.image:getWidth(), self.image:getHeight())

		--setup quad list
		self.quad = {}
		local pgrid = grid+vector(pad, pad)
		for y = 0, math.floor(size.y/pgrid.y)-1 do
			for x = 0, math.floor(size.x/pgrid.x)-1 do
				table.insert(self.quad, love.graphics.newQuad(x*pgrid.x, y*pgrid.y, grid.x, grid.y, size.x, size.y))
			end
		end
	end
} 