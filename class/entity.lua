--[[
	Generic entity class. Can collide with the map as well as other Rect-derived classes.
	Simple pseudo physics are also simulated (velocity, gravity, etc)

	The thin vector is used to narrow collision rect when checking y axis, allowing the player to fit through
	small gaps and preventing issues when colliding with the corner of a tile.
]]
local map2 = Map("test.map")
local thin = vector(1, 1)
local thin2 = vector(2, 1)

class "Entity" (Rect) {
	__init__ = function(self, position)
		Rect.__init__(self, position.x, position.y, 14, 24)

		self.accel = 2000
		self.gravity = 800
		self.friction = 1000
		self.vel = vector()
		self.maxVel = vector(125, 300)
	end,

	update = function(self, dt, map)
		--clamp velocity to max values
		self.vel.x = math.clamp(-self.maxVel.x, self.vel.x, self.maxVel.x)
		self.vel.y = math.clamp(-self.maxVel.y, self.vel.y, self.maxVel.y)

		--check tilemap collision and move entity
		self.pos.x = self.pos.x + self.vel.x * dt
		self:collideMapX(map)

		self.pos.y = self.pos.y+self.vel.y*dt
		self:collideMapY(map)

		--velocity pseudo physics
		self:applyGravity(dt)
		self:applyFriction(dt)

		--debug
		self.test = map:getSample(self.pos:clone(), self.size-vector(1,1))
	end,

	draw = function(self)
		--debug stuff
		Rect.draw(self)	
		-- for y, i in ipairs(self.test) do
		-- 	for x, j in ipairs(i) do
		-- 		love.graphics.setColor(0, 255, 0, 128)
		-- 		love.graphics.rectangle("fill", j.pos.x, j.pos.y, 16, 16)
		-- 		love.graphics.setColor(255, 255, 255, 255)
		-- 		love.graphics.print(j.id, j.pos.x+4, j.pos.y+8)
		-- 	end
		-- end
		love.graphics.print("velocity " .. tostring(self.vel), 1, 580)
		love.graphics.print("world pos " .. tostring(self.pos), 1, 560)
		love.graphics.print("tile pos " .. tostring(map2:worldToTile(self.pos:clone())), 1, 540)
	end,

	applyGravity = function(self, dt)
		self.vel.y = self.vel.y+self.gravity*dt
	end,

	applyFriction = function(self, dt)
		if self.vel.x > 0 then
				self.vel.x = self.vel.x > self.friction*dt and self.vel.x-self.friction*dt or 0
		elseif self.vel.x < 0 then
				self.vel.x = self.vel.x < -self.friction*dt and self.vel.x+self.friction*dt or 0
		end
	end,

	collideMapX = function(self, map)
		local sample = map:getSample(self.pos, self.size-vector(0, 1)) --area of the map the rect overlaps, reduce by 1 to prevent rounding issues
		local finish = #sample[1]
		for i = 1, #sample do
			local l, r = sample[i][1], sample[i][finish] --iterate over desired tiles, 1 pair at a time
			local left = l.solid == true and l or left --check collision (REDO when tiles are implemented)
			local right = r.solid == true and r or right
			if left then
				self.pos.x = left.pos.x+map.tileSize --snap to tile
				self.vel.x = 0
			end
			if right then 
				self.pos.x = right.pos.x-self.size.x
				self.vel.x = 0
			end
		end

		
	end,

	collideMapY = function(self, map)
		local sample = map:getSample(self.pos, self.size-vector(1,0))
		local finish = #sample
		for i = 1, #sample[1] do
			local c, f = sample[1][i], sample[finish][i]
			local ceil = c.solid == true and c or ceil
			local floor = f.solid == true and f or floor

			if ceil and self.vel.y < 0 then 
			self.pos.y = ceil.pos.y+map.tileSize
			self.vel.y = 0
			end
			if floor and self.vel.y > 0 then 
				self.pos.y = floor.pos.y-self.size.y
				self.vel.y = 0
				if self.jump then
					self.jumpCurrent = 0
				end
			end
		end

		
	end

}