--[[
	Rectangle class intended for use as a base for other classes.
	Can check collision with other Rects or with a single point.
]]

local x1, x2, y1, y2, w1, w2, h1, h2

class "Rect" {
	__init__ = function(self, x, y, w, h)
		self.pos = type(x) == "table" and x or vector(x, y)
		self.size = type(x) == "table" and y or vector(w, h)
		self.center = (self.pos+self.size)*0.5
	end,

	draw = function(self)
		local p, s = self.pos, self.size
		love.graphics.setColor(255, 128, 128)
		love.graphics.rectangle("fill", p.x, p.y, s.x, s.y)
	end,

	collide = function(self, rec2)
		x1, y1 = self.pos:unpack()
		w1, h1 = self.size:unpack()
		x2, y2 = rec2.pos:unpack()
		w2, h2 = rec2.size:unpack()
		return not(x1+w1 < x2 or x2+w2 < x1 or y1+h1 < y2 or y2+h2 < y1)
	end,

	collidePoint = function(self, point)
		x1, y1 = self.pos:unpack()
		w1, h1 = self.size:unpack()
		x2, y2 = point:unpack()
		return not(x2 < x1 or x1+w1 < x2 or y2 < y1 or y1+h1 < y2)
	end,

	distance = function(self, point)
		return vector.dist(self.pos, point)
	end,

	__str__ = function(self)
		return "Rect:\n\tx\t" .. self.pos.x .. "\n\tx2\t" .. self.pos.x+self.size.x .. "\n\ty\t" .. self.pos.y .. "\n\ty2\t" .. self.pos.y+self.size.y 
	end
}