--[[
	Main Game Structure
	Game manages the current state of the game.

	It should interpret input into key binds and manage network connections for the state.
]]

require "class.state"

local stateList = {}
local curState = State()
love.handlers.changeState = function(s)
	assert(stateList[s], "Invalid state, cannot change to " .. s)
	curState = stateList[s]()
end

class "Game" {
	__init__ = function(self)
		--load states
		for _, file in ipairs(love.filesystem.getDirectoryItems("state")) do
			local state = file:match("^(.+)%.lua$")
			if state then
				stateList[state] = require("state." .. state)
			end
		end

		love.event.push("changeState", "testState")
		love.graphics.setDefaultFilter("nearest")
	end,
	
	update = function(self, dt)
		curState:update(dt)
	end,

	draw = function(self)
		love.graphics.push()
		love.graphics.scale(1)
		curState:draw()
		love.graphics.pop()
	end,

	--STATE CALLBACK FUNCTIONS

	--m&k
	textinput = function(self, text)
		curState:textinput(text)
	end,

	keypressed = function(self, key, unicode)
		curState:keypressed(key, unicode)
	end,

	keyreleased = function(self, key)
		curState:keyreleased(key)
	end,

	mousepressed = function(self, x, y, button)
		curState:mousepressed(x, y, button)
	end,

	mousereleased = function(self, x, y, button)
		curState:mousereleased(x, y, button)
	end,

	mousefocus = function(self, f)
		curState:mousefocus(f)
	end,

	--controller
	joystickadded = function(self, joystick)
		curState:joystickadded(joystick)
	end,

	joystickremoved = function(self, joystick)
		curState:joystickremoved(joystick)
	end,

	gamepadpressed = function(self, joystick, button)
		curState:gamepadpressed(joystick, button)
	end,

	gamepadreleased = function(self, joystick, button)
		curState:gamepadreleased(joystick, button)
	end,

	--application
	focus = function(self, f)
		curState:focus(f)
	end,

	visible = function(self, v)
		curState:visible(v)
	end,

	quit = function(self)
		curState:quit()
	end
}