require "class.atlas"

class "Tileset" (Atlas) {
	__init__ = function(self, fileName)
		local file = love.filesystem.load(fileName)()
		assert(file, "Tileset " .. fileName .. " could not be found.")
		Atlas.__init__(self, file.image, file.grid, file.pad)

		self.tileData = file.tileData
		self.tileData[0] = {solid = false}
	end
}