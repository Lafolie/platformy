class "State" {
	__init__ = function(self)
	end,

	__preCache = function(self)
	end,

	load = function(self)
	end,

	update = function(self, dt)
	end,

	update = function(self, dt)
	end,

	draw = function(self)
	end,

	--m&k
	textinput = function(self, text)
	end,

	keypressed = function(self, key, unicode)
	end,

	keyreleased = function(self, key)
	end,

	mousepressed = function(self, x, y, button)
	end,

	mousereleased = function(self, x, y, button)
	end,

	mousefocus = function(self, f)
	end,

	--controller
	joystickadded = function(self, joystick)
	end,

	joystickremoved = function(self, joystick)
	end,

	gamepadpressed = function(self, joystick, button)
	end,

	gamepadreleased = function(self, joystick, button)
	end,

	--application
	focus = function(self, f)
	end,

	visible = function(self, v)
	end,

	quit = function(self)
	end
}