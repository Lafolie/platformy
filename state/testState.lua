--testing statification

require "class.rect"
require "class.map"
require "class.entity"
require "class.character"

class "TestState" (State) {
	__init__ = function(self)
		self.r = Rect(32, 32, 16, 16)
		self.s = Character(vector(64, 64))
		self.msg = "Yo!"
		self.map = Map("test.map")
	end,

	update = function(self, dt)
		self.s:update(dt, self.map)

		self.msg = self.r:collide(self.s) and "Hit" or "Miss"
	end,

	draw = function(self)
		love.graphics.setColor(255, 255, 255)
		self.map:draw()
		love.graphics.print(self.msg, 16, 16)
		self.r:draw()
		self.s:draw()
		love.graphics.setColor(0, 255, 0)
		love.graphics.print(love.timer.getFPS(), 1, 1)
	end,

	--everything below this point is haxx
	keypressed = function(self, key, unicode)
		if key == "escape" then love.event.push("quit") end
		if key == " " then 
			self.s.input.jump = true
		end
		if key == "return" then pause = not pause end
		if key == "a" then self.s.input.left = not self.s.input.left end
		if key == "d" then self.s.input.right = not self.s.input.right end
	end,

	keyreleased = function(self, key)
		if key == " " then 
			self.s.input.jump = not self.s.input.jump 
			self.s:jumpReleased()
		end
		if key == "d" then self.s.input.right = not self.s.input.right end
		if key == "a" then self.s.input.left = not self.s.input.left end

	end
}

return TestState