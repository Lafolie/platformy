function love.conf(t)
	--application settings
	t.identity = "platformy"
	t.window.title = "Platformy"
	t.version = "0.9.1"
	t.console = true

	--video settings
	t.window.vsync = true

	--module settings
	t.modules.physics = false
	
end